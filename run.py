# -*- coding:utf-8 -*-
"""
@Time        : 2024/3/2
@File        : run.py
@Author      : lyz
@version     : python 3
@Description : 打包并上传至 pypi
"""
import subprocess


def package_and_upload_to_pypi():
    # 升级打包工具
    subprocess.run("pip install --upgrade setuptools wheel twine", shell=True, check=True)
    # 打包
    subprocess.run("python setup.py sdist bdist_wheel", shell=True, check=True)
    # 检查包
    subprocess.run("twine check dist/*", shell=True, check=True)
    # 上传包到 PyPI
    subprocess.run("twine upload --repository pypi --username __token__ --password pypi-AgEIcHlwaS5vcmcCJDAxYzViZmU2LW"
                   "Y0NzMtNDBlNy04NTczLTAxYTA0NGQ2NzhkMAACKlszLCI1ZTE4ZTAwZS1hMTc5LTRlNjAtYmIxNi02MWM0Y2E4OTgxMzYiXQAA"
                   "BiDf7ptiLswKlVuBsQUOLb99PUPJ6sif6zyf4BPPXKVQlg dist/*", shell=True, check=True)
    print("Package upload to PyPI successful!")


if __name__ == '__main__':
    package_and_upload_to_pypi()
