# Python Personalized Kits

# 介绍
python个人定制工具包

# 软件架构
setup:打包项目
run:打包并上传

# 安装教程
1. pip install pyutilitytool
2. 修改setup的version/install_requires/classifiers
3. 运行run

# 使用说明
1.  如果 pip install pyutilitytool 无法安装，则使用 pip install -r requirements.txt
2.  kits 为工具包
